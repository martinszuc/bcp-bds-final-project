package org.but.feec.eshop.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.eshop.api.CategoryView;
import org.but.feec.eshop.api.ProductEditView;
import org.but.feec.eshop.api.ProductInsertView;
import org.but.feec.eshop.data.ProductRepository;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.catalog.Catalog;
import java.util.Optional;

public class InsertProductController {

    private static final Logger logger = LoggerFactory.getLogger(InsertProductController.class);

    @FXML
    private TextField brandTextField;
    @FXML
    private ComboBox<String> categoryBox;
    @FXML
    private TextField descTextField;
    @FXML
    private Button insertProductButton;
    @FXML
    private TextField priceTextField;
    @FXML
    private TextField quantityTextField;
    @FXML
    private TextField sizeTextField;
    @FXML
    private TextField titleTextField;
    @FXML
    void handleInsertProductButton(ActionEvent event) {

        String category = categoryBox.getValue();
        String brand = brandTextField.getText();
        String title = titleTextField.getText();
        String size = sizeTextField.getText();
        String desc = descTextField.getText();
        String quantity = quantityTextField.getText();
        Double price = Double.valueOf(priceTextField.getText());

        ProductInsertView productInsertView = new ProductInsertView();

        productInsertView.setBrand(brand);
        productInsertView.setCategory(category);
        productInsertView.setDesc(desc);
        productInsertView.setPrice(price);
        productInsertView.setQuantity(quantity);
        productInsertView.setSize(size);
        productInsertView.setTitle(title);

        productRepository.insertProduct(productInsertView);

        personInsertedConfirmationDialog();
    }

    private ProductRepository productRepository;
    private ValidationSupport validation;
    public Stage stage;
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        productRepository = new ProductRepository();

        validation = new ValidationSupport();
        validation.registerValidator(categoryBox, Validator.createEmptyValidator("The category must not be empty."));
        validation.registerValidator(brandTextField, Validator.createEmptyValidator("The brand must not be empty."));
        validation.registerValidator(titleTextField, Validator.createEmptyValidator("The title name must not be empty."));
        validation.registerValidator(sizeTextField, Validator.createEmptyValidator("The size must not be empty."));
        validation.registerValidator(descTextField, Validator.createEmptyValidator("The desc must not be empty."));
        validation.registerValidator(quantityTextField, Validator.createEmptyValidator("The quantity must not be empty."));
        validation.registerValidator(priceTextField, Validator.createEmptyValidator("The price must not be empty."));

        insertProductButton.disableProperty().bind(validation.invalidProperty());

        categoryBox.getItems().addAll(
                "T-Shirts",
                "Long sleeve T-Shirts",
                "Shirts",
                "Hoodies",
                "Jackets",
                "Pants",
                "Jeans",
                "Shorts",
                "Shoes");
        logger.info("Product insert controller initialized");
       // personInsertedConfirmationDialog();
       // stage.close();
    }
    private void personInsertedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Product Insert Confirmation");
        alert.setHeaderText("Your product was successfully inserted.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }
}
