package org.but.feec.eshop.api;
import javafx.beans.property.*;

public class ProductDetailedView {

        private LongProperty id = new SimpleLongProperty();
        private StringProperty category = new SimpleStringProperty();
        private StringProperty brand = new SimpleStringProperty();
        private StringProperty title = new SimpleStringProperty();
        private StringProperty size = new SimpleStringProperty();
        private StringProperty desc = new SimpleStringProperty();
        private StringProperty quantity = new SimpleStringProperty();
        private DoubleProperty price = new SimpleDoubleProperty();
        private StringProperty review = new SimpleStringProperty();
        private StringProperty rating = new SimpleStringProperty();
        private LongProperty iduser = new SimpleLongProperty();

    public Long getId(){return idProperty().get();}
    public void setId(Long id) {this.idProperty().setValue(id);}
    public String getTitle(){return titleProperty().get();}
    public void setTitle(String title){this.titleProperty().setValue(title);}
    public String getCategory(){return categoryProperty().get();}
    public void setCategory(String category){this.categoryProperty().setValue(category);}
    public String getBrand(){return brandProperty().get();}
    public void setBrand(String brand){this.brandProperty().setValue(brand);}
    public String getSize(){return sizeProperty().get();}
    public void setSize(String size){this.sizeProperty().setValue(size);}
    public String getDesc(){return descProperty().get();}
    public void setDesc(String desc){this.descProperty().setValue(desc);}
    public String getQuantity(){return qtyProperty().get();}
    public void setQuantity(String quantity){this.qtyProperty().setValue(quantity);}
    public Double getPrice(){return priceProperty().get();}
    public void setPrice(Double price){this.priceProperty().setValue(price);}
    public String getReview(){return reviewProperty().get();}
    public void setReview(String review){this.reviewProperty().setValue(review);}
    public String getRating(){return ratingProperty().get();}
    public void setRating(String rating){this.ratingProperty().setValue(rating);}
    public Long getIdUser(){return idUserProperty().get();}
    public void setIdUser(Long id) {this.idUserProperty().setValue(id);}

    public LongProperty idProperty(){return id;}
    public StringProperty titleProperty(){return title;}
    public StringProperty sizeProperty(){return size;}
    public StringProperty categoryProperty(){return category;}
    public StringProperty brandProperty(){return brand;}
    public StringProperty descProperty(){return desc;}
    public StringProperty qtyProperty(){return quantity;}
    public DoubleProperty priceProperty(){return price;}
    public StringProperty reviewProperty(){return review;}
    public StringProperty ratingProperty(){return rating;}
    public LongProperty idUserProperty(){return iduser;}
}
