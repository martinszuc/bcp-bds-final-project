package org.but.feec.eshop.api;

import javafx.beans.property.*;

public class ProductView {
    private LongProperty id = new SimpleLongProperty();
    private StringProperty category = new SimpleStringProperty();
    private StringProperty brand = new SimpleStringProperty();
    private StringProperty title = new SimpleStringProperty();
    private StringProperty size = new SimpleStringProperty();
    private StringProperty desc = new SimpleStringProperty();
    private StringProperty quantity = new SimpleStringProperty();
    //private StringProperty discount = new SimpleStringProperty();
    //private BooleanProperty ready = new SimpleBooleanProperty();
    private DoubleProperty price = new SimpleDoubleProperty();

    public Long getId(){return idProperty().get();}
    public void setId(Long id) {this.idProperty().setValue(id);}
    public String getTitle(){return titleProperty().get();}
    public void setTitle(String title){this.titleProperty().setValue(title);}
    public String getCategory(){return categoryProperty().get();}
    public void setCategory(String category){this.categoryProperty().setValue(category);}
    public String getBrand(){return brandProperty().get();}
    public void setBrand(String brand){this.brandProperty().setValue(brand);}
    public String getSize(){return sizeProperty().get();}
    public void setSize(String size){this.sizeProperty().setValue(size);}
    public String getDesc(){return descProperty().get();}
    public void setDesc(String desc){this.descProperty().setValue(desc);}
    public String getQuantity(){return qtyProperty().get();}
    public void setQuantity(String quantity){this.qtyProperty().setValue(quantity);}
    //public String getDiscount(){return discountProperty().get();}
    //public void setDiscount(String discount){this.discountProperty().setValue(discount);}
    public Double getPrice(){return priceProperty().get();}
    public void setPrice(Double price){this.priceProperty().setValue(price);}
    //public Boolean getReady(){return readyProperty().get();}
    //public void setReady(Boolean ready){this.readyProperty().setValue(ready);}

    public LongProperty idProperty(){return id;}
    public StringProperty titleProperty(){return title;}
    public StringProperty sizeProperty(){return size;}
    public StringProperty categoryProperty(){return category;}
    public StringProperty brandProperty(){return brand;}
    public StringProperty descProperty(){return desc;}
    public StringProperty qtyProperty(){return quantity;}
    //public StringProperty discountProperty(){return discount;}
    public DoubleProperty priceProperty(){return price;}
    //public BooleanProperty readyProperty(){return ready;}

}
