package org.but.feec.eshop.api;

public class CategoryView {
    private Long id;
    private String title;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getCategory(){return this.title;}
    public void setCategory(String category){this.title=category;}
}
